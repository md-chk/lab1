/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightservation.domain;

/**
 *
 * @author Room107
 */
public class Trip {
    private AirPort fromAirPort ;
    private AirPort toAirPort ;
    private byte noOfPassengers ;
    private ZonedDateTime departureDateTime ;
    private ZonedDateTime returnDateTime ;
    private boolean directFlight ;
    private TravelClass travelClass;
}
