/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightservation.domain;

/**
 *
 * @author Room107
 */
public enum AirPort {
    BKK("Suvarnabhumi Airport","Bangkok","TH"),
   DMK("Don Mueng International Airport","Bangkok","TH")
    ,HKT("Phuket International Airport","Phuket","TH")
    ,CNX("Chianf Mai International Airport","Chiang Mai","TH")
    ,HDY("Hat Yai International Airport","Songkla","TH")
    ,CEI("Mae Fah Luang Chiang Rai International Airport","Chiang Rai","TH");
    
    AirPort(String airport,String city,String country){
        this.airportName = airport;
        this.city = city;
        this.country = country;
    }
    
    private final String airportName;
    private final String city;
    private final String country;
    
    
    public String city(){
        return city;
    }
    
    public String country(){
        return country;
    }
    
    public String airportName(){
        return airportName;
    }
    
    
}
